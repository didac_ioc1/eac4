/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package didac_ioc.eac4;
/**
 * He aprés molt fent el mòdul 8 de DAW encara que hagi estat dur!!!
 */

/**
 * Aquesta classe proporciona mètodes estàtics per realitzar operacions matemàtiques bàsiques. 
 * 
 * @author Dídac Padilla Marsiñach
 * @version 1.0
 * @since 1.0
 */
public class Operacions {

    /**
     * Mètode per sumar dos nombres.
     * 
     * @param a el primer nombre a sumar
     * @param b el segon nombre a sumar
     * @return la suma dels dos nombres
     */
    public static double sumar(double a, double b) {
        return a + b;
    }

    /**
     * Mètode per restar dos nombres.
     * 
     * @param a el nombre al qual se li resta
     * @param b el nombre que es resta
     * @return la diferència entre a i b
     */
    public static double restar(double a, double b) {
        return a - b;
    }

    /**
     * Mètode per multiplicar dos nombres.
     * 
     * @param a el primer nombre a multiplicar
     * @param b el segon nombre a multiplicar
     * @return el producte dels dos nombres
     */
    public static double multiplicar(double a, double b) {
        return a * b;
    }

    /**
     * Mètode per dividir dos nombres.
     * 
     * @param a el dividend
     * @param b el divisor
     * @return el resultat de la divisió a / b
     * @throws ArithmeticException si el divisor és zero
     */
    public static double dividir(double a, double b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException("El divisor no pot ser zero");
        }
        return a / b;
    }
}

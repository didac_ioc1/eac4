/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package didac_ioc.eac4;

import java.util.Scanner;

/**
 * Aquesta classe implementa una calculadora.
 * 
 * @author Dídac Padilla Marsiñach
 * @version 1.0
 * @since 1.0
 */

public class EAC4 {

    /**
     * Mètode que demana dos nombres i retorna el resultat de diverses operacions matematiques.
     * 
     * @param args els arguments del programa.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix el primer número: ");
        double num1 = scanner.nextDouble();

        System.out.println("Introdueix el segon número: ");
        double num2 = scanner.nextDouble();

        System.out.println("Suma: " + Operacions.sumar(num1, num2));
        System.out.println("Resta: " + Operacions.restar(num1, num2));
        System.out.println("Multiplicació: " + Operacions.multiplicar(num1, num2));

        try {
            System.out.println("Divisió: " + Operacions.dividir(num1, num2));
        } catch (ArithmeticException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            scanner.close(); 
        }
    }
    
}

